import requests
import urllib.request as req
from bs4 import BeautifulSoup

URL = 'https://en.wikipedia.org/w/index.php?search='

def main():
    query: str = input('Enter Keyword or q to quit: ')
    while query != 'q':

        r = requests.get(URL + query)
        soup = BeautifulSoup(r.text, 'html.parser')

        headers = soup.find_all('h1', attrs={'class':'firstHeading'})
        body = soup.find_all('p')

        for h in headers:
            print(h.text)

        for b in body:
            print(b.text)

        print('\n\n')

        query = input('Enter Keyword or q to quit: ')
main()
